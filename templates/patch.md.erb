<!--

If you make any changes to this template in Release Tools, also make sure to
update any existing release issues (if necessary).

-->

## Preparation

- Preparation MR's should already be created
- [ ] Ensure `<%= version.stable_branch(ee: true) %>` [GitLab branch] is green.
- [Ensure any backports targeting] <%= version %> are merged to their stable counter part
  - [ ] [GitLab Backports](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: true) %>)
  - [ ] [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>)
  - [ ] [CNG](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>)
  - [ ] [Gitaly](https://gitlab.com/gitlab-org/gitaly/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>)
- [ ] Perform automated merging into the preparation branches:
    ```sh
    # In Slack
    /chatops run release merge <%= version.to_ce %>
    ```
- Check for any MR's that might have been created that are targeting our preparation branch
  - [ ] [GitLab](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: true) %>-patch-<%= version.patch %>)
  - [ ] [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>-patch-<%= version.patch %>)
  - [ ] [CNG](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>-patch-<%= version.patch %>)
  - [ ] [Gitaly](https://gitlab.com/gitlab-org/gitaly/-/merge_requests?scope=all&state=opened&target_branch=<%= version.stable_branch(ee: false) %>-patch-<%= version.patch %>)
- Merge the preparation [merge requests](#related-merge-requests)
  - [ ] `gitlab-org/gitlab`
  - [ ] `gitlab-org/omnibus-gitlab`
  - [ ] `gitlab-org/build/CNG`
- Check the following list of critical issues/MRs which are to be included in `<%= version %>`.
  - [ ] REFERENCE_TO_MR_TO_PICK
- [ ] Ensure that any post-deploy migrations in the stable branch have been executed on GitLab.com by [executing the post-deploy migration pipeline]:
  `/chatops run post_deploy_migrations execute`.
- [ ] Ensure builds are green on [Omnibus]

### Backport request

If this patch release is part of a backport request, we will not be able to deploy the package to our release instance. Now is the time to kick off QA. Refer to the [backport QA testing] documentation to complete this task:

- [ ] QA is complete, and sign-off by the current QA on-call is noted as a comment on this issue.

[Ensure any backports targeting]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/faq.md#backports-for-prior-versions
[preparation MRs]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/picking-into-merge-requests.md
[remaining merge requests]: https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests?scope=all&state=merged&label_name[]=Pick%20into%20<%= version.to_minor %>
[Omnibus]: https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch %>
[GitLab branch]: https://gitlab.com/gitlab-org/gitlab/commits/<%= version.stable_branch(ee: true) %>
[backport QA testing]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/backport-qa-testing.md

## Packaging

- [ ] Check if mirroring synced stable branches to dev. If the output is :white_check_mark: for every repo, we can proceed to tag.
      Note. If GitLab Canonical to Security mirroring has diverged due to security merges this mirror is expected to show as a broken and can be safely ignored.

   ```
   # In Slack
   /chatops run mirror status
   ```
- [ ] Tag `<%= version %>`:
   ```sh
   # In Slack:
   /chatops run release tag <%= version %>
   ```
- [ ] While waiting for packages to build, now is a good time to [prepare the blog post]. Look at previous MRs for examples. => BLOG_POST_MR
- [ ] Check progress of [EE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>) and [CE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: false) %>).
    - `/chatops run release status <%= version %>`
    - This might take a while (around 80 min).
    - We only need the EE packages to finish to continue with next steps.

[prepare the blog post]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/patch/blog-post.md

## Deploy

For patch releases, the only available environment for deploys is `release.gitlab.net`. All GitLab Inc. team members can login to that installation using their email address (through google oauth).

Deployment to release.gitlab.net is not required if the patch release is not for the latest completed monthly release version.
This is because release.gitlab.net will be on the latest monthly release, and
deploying a patch of an older version will require rolling back release.gitlab.net.

### release.gitlab.net

Deployments to release.gitlab.net are performed automatically.

<details>
  <summary>Instructions to manually deploy if required.</summary>

If you need to manually run a deployment, you can do so as follows:

```sh
# In Slack:
/chatops run deploy <%= version %>-ee.0 release
```

</details>

## Release

- [ ] Publish the packages via ChatOps:
   ```
   # In Slack:
   /chatops run publish <%= version %>
   ```
- [ ] Verify the `check-packages` job completes successfully on the [EE Pipeline](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>)
- [ ] Verify the `check-packages` job completes successfully on the [CE Pipeline](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: false) %>)
- [ ] Verify that Docker images appear on `hub.docker.com`: [EE](https://hub.docker.com/r/gitlab/gitlab-ee/tags) / [CE](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
- [ ] Deploy the blog post
- [ ] Create the `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>)

## References

### gitlab.com

- https://gitlab.com/gitlab-org/gitlab-foss/commits/<%= version.stable_branch %>
- https://gitlab.com/gitlab-org/gitlab/commits/<%= version.stable_branch(ee: true) %>
- https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch %>

### dev.gitlab.org

- https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
- https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

## Release Certification

The [release certification process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html) may apply to this release. cc @gitlab-com/gl-security/federal-application-security

[executing the post-deploy migration pipeline]: https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration#how-to-execute-post-deploy-migrations

/milestone %"<%= version.to_minor %>"
/due in 7 days
