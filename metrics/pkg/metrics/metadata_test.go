package metrics

import (
	"testing"
)

func TestCheckLabels(t *testing.T) {
	labels := []string{"color", "level"}
	allowedValues := [][]string{
		{"green", "yellow", "red"},
		{"high", "medium", "low"},
	}

	examples := []struct {
		name          string
		values        []string
		labels        []string
		allowedValues [][]string
		error         string
	}{
		{
			name:          "1 label ok",
			values:        []string{"yellow"},
			labels:        labels[0:1],
			allowedValues: allowedValues[0:1],
		},
		{
			name:          "1 label ko",
			values:        []string{"not_a_color"},
			labels:        labels[0:1],
			allowedValues: allowedValues[0:1],
			error:         `"not_a_color" is not a valid "color" value`,
		},
		{
			name:          "2 labels ok",
			values:        []string{"yellow", "high"},
			labels:        labels,
			allowedValues: allowedValues,
		},
		{
			name:          "2 labels - first ko",
			values:        []string{"not_a_color", "high"},
			labels:        labels,
			allowedValues: allowedValues,
			error:         `"not_a_color" is not a valid "color" value`,
		},
		{
			name:          "2 labels - second ko",
			values:        []string{"red", "foo"},
			labels:        labels,
			allowedValues: allowedValues,
			error:         `"foo" is not a valid "level" value`,
		},
		{
			name:          "missing label values",
			values:        []string{"red"},
			labels:        labels,
			allowedValues: allowedValues,
			error:         `Expected 2 labels`,
		},
		{
			name:          "too many label values",
			values:        []string{"red", "high", "extra"},
			labels:        labels,
			allowedValues: allowedValues,
			error:         `Expected 2 labels`,
		},
	}

	for _, example := range examples {
		t.Run(example.name, func(tt *testing.T) {
			meta := description{
				labels:      example.labels,
				labelValues: example.allowedValues,
			}
			err := meta.CheckLabels(example.values)

			if example.error == "" && err == nil {
				return
			}

			if example.error == "" && err != nil {
				tt.Fatalf("Unexpected error, got: %q", err)
			}

			if err == nil {
				tt.Fatalf("Expected error %q, got nil", example.error)
			}

			if err.Error() != example.error {
				tt.Fatalf("Expected error %q, got %q", example.error, err)
			}
		})
	}
}

func TestWithCarsesianProductLabelReset(t *testing.T) {
	examples := []struct {
		name           string
		opts           []MetricOption
		expectedValues [][]string
	}{
		{
			name:           "no labels",
			expectedValues: nil,
		},
		{
			name: "1 label",
			opts: []MetricOption{WithLabel("severity", []string{"low", "high"})},
			expectedValues: [][]string{
				{"low"},
				{"high"},
			},
		},
		{
			name: "2 labels",
			opts: []MetricOption{
				WithLabel("severity", []string{"low", "high"}),
				WithLabel("environment", []string{"staging", "production"}),
			},
			expectedValues: [][]string{
				{"low", "staging"},
				{"low", "production"},
				{"high", "staging"},
				{"high", "production"},
			},
		},
		{
			name: "3 labels",
			opts: []MetricOption{
				WithLabel("severity", []string{"low", "high"}),
				WithLabel("environment", []string{"staging", "production"}),
				WithLabel("service", []string{"test"}),
			},
			expectedValues: [][]string{
				{"low", "staging", "test"},
				{"low", "production", "test"},
				{"high", "staging", "test"},
				{"high", "production", "test"},
			},
		},
	}

	for _, example := range examples {
		t.Run(example.name, func(tt *testing.T) {
			example.opts = append(example.opts, WithCartesianProductLabelReset())
			descOpts := applyMetricOptions(example.opts)

			if len(descOpts.labelsToInitialize) != len(example.expectedValues) {
				tt.Fatalf("Expected %v label set, got %v", len(example.expectedValues), len(descOpts.labelsToInitialize))
			}

			for i, labels := range descOpts.labelsToInitialize {
				expected := example.expectedValues[i]
				if len(labels) != len(expected) {
					tt.Errorf("At index %v expected %v label values, got %v", i, len(expected), len(labels))
				}

				for j, labelValue := range labels {
					if labelValue != expected[j] {
						tt.Errorf("At index %v expected %q, got %q", j, expected[j], labelValue)
					}
				}
			}
		})
	}
}
