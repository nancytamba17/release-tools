package pipelines

import (
	"context"
	"fmt"
	"regexp"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

const (
	// limit limits the number of fetched pipelines
	limit int = 20

	subsystem string = "deployment"

	apiErrorLabel = "api_error"

	releaseToolsProject = "gitlab-org/release/tools"
)

var (
	// tickerInterval represents the scraping frequency
	tickerInterval = 5 * time.Minute
	// clientTimeout is the timeout for fetching pipelines
	clientTimeout = 10 * time.Second

	// productVersionTag matches the expected product version tag on release-tools
	productVersionTag = regexp.MustCompile(`\d+\.\d+\.\d{12}`)

	statusLabels = []string{
		string(gitlab.Running),
		"scheduled",
		string(gitlab.Failed),
		string(gitlab.Manual),
		"unknown",
	}
	errorLabels = []string{apiErrorLabel}
)

type observer struct {
	ctx    context.Context
	ops    pipelinesClient
	log    *logrus.Logger
	metric metrics.Gauge

	errorMetric metrics.Counter
}

func Observe(ctx context.Context, log *logrus.Logger, token string) error {
	metric, err := initMetric()
	if err != nil {
		return fmt.Errorf("can't init the pipelines_total metric: %w", err)
	}

	errorMetric, err := initErrorMetric()
	if err != nil {
		return fmt.Errorf("can't init the pipelines_observer_errors metric: %w", err)
	}

	client, err := newClient(token)
	if err != nil {
		return fmt.Errorf("can't create a gitlab client: %w", err)
	}

	obs := observer{
		ctx:         ctx,
		ops:         client,
		log:         log,
		metric:      metric,
		errorMetric: errorMetric,
	}

	go obs.mainLoop()

	return nil
}

func initMetric() (metrics.Gauge, error) {
	return metrics.NewGaugeVec(
		metrics.WithName("pipelines_total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Total number of coordinated pipelines since the last successful deployment"),
		metrics.WithLabel("status", statusLabels),
		metrics.WithCartesianProductLabelReset(),
	)
}

func initErrorMetric() (metrics.Counter, error) {
	return metrics.NewCounterVec(
		metrics.WithName("pipelines_observer_errors"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Total number of errors from the pipelines observer"),
		metrics.WithLabel("reason", errorLabels),
		metrics.WithCartesianProductLabelReset(),
	)
}

func (o *observer) mainLoop() {
	ticker := time.NewTicker(tickerInterval)

	// init metrics values on boot, then continue with the ticker
	o.fetchAndUpdate()

	for {
		select {
		case <-o.ctx.Done():
			return
		case <-ticker.C:
			o.fetchAndUpdate()
		}
	}
}

func (o *observer) fetchAndUpdate() {
	pipelines, err := o.fetchPipelines()
	if err != nil {
		o.log.WithError(err).Error("can't fetch pipelines")
		o.errorMetric.Inc(apiErrorLabel)

		if len(pipelines) == 0 {
			return
		}
	}

	o.updateMetrics(pipelines)
}

func (o *observer) fetchPipelinesPage(page int, pipelines *[]*gitlab.PipelineInfo) (nextPage int, e error) {
	reqCtx, cancel := context.WithTimeout(o.ctx, clientTimeout)
	defer cancel()

	opts := &gitlab.ListProjectPipelinesOptions{
		Source: gitlab.String("push"),
		ListOptions: gitlab.ListOptions{
			Page:    page,
			PerPage: limit,
		},
	}

	batch, response, err := o.ops.ListProjectPipelines(
		releaseToolsProject,
		opts,
		gitlab.WithContext(reqCtx))

	if err != nil {
		o.log.WithError(err).Error("api failure")
		return 0, err
	}

	for _, pipeline := range batch {
		pipelineLogger := o.log.WithFields(logrus.Fields{
			"ref":    pipeline.Ref,
			"status": pipeline.Status,
			"source": pipeline.Source,
		})

		if !productVersionTag.MatchString(pipeline.Ref) {
			pipelineLogger.Trace("skip pipeline - not a product version tag")
			continue
		}

		if pipeline.Status == string(gitlab.Success) {
			pipelineLogger.WithFields(logrus.Fields{
				"url":        pipeline.WebURL,
				"created_at": pipeline.CreatedAt,
			}).Info("Last successful pipeline")

			return 0, nil
		}

		pipelineLogger.Trace("appending")
		*pipelines = append(*pipelines, pipeline)
	}

	return response.NextPage, nil
}

func (o *observer) fetchPipelines() ([]*gitlab.PipelineInfo, error) {
	pipelines := make([]*gitlab.PipelineInfo, 0)

	nextPage := 0
	var err error

	for nextPage, err = o.fetchPipelinesPage(0, &pipelines); nextPage != 0 && err == nil; {
		nextPage, err = o.fetchPipelinesPage(nextPage, &pipelines)
	}

	return pipelines, err
}

func (o *observer) updateMetrics(pipelines []*gitlab.PipelineInfo) {
	counters := make(map[string]float64)
	for _, status := range statusLabels {
		counters[status] = 0
	}

	for _, pipeline := range pipelines {
		status := pipeline.Status

		pipelineLogger := o.log.WithFields(logrus.Fields{
			"ref":    pipeline.Ref,
			"status": status,
		})
		pipelineLogger.Info(pipeline.WebURL)

		if err := o.metric.CheckLabels([]string{pipeline.Status}); err != nil {
			pipelineLogger.WithError(err).Warn("unknown status")
			status = "unknown"
		}

		counters[status]++
	}

	for status, count := range counters {
		o.metric.Set(count, status)
	}
}
