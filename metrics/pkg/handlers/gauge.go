package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

type gauge struct {
	metric metrics.Gauge
}

func NewGauge(metric metrics.Gauge) Pluggable {
	return &gauge{metric}
}

func (c *gauge) PlugRoutes(r *mux.Router) {
	subRouter := r.PathPrefix(route(c.metric)).Subrouter()

	subRouter.HandleFunc("/inc", c.incHandlerFunc)
	subRouter.HandleFunc("/set", c.setHandlerFunc)
}

func (c *gauge) incHandlerFunc(w http.ResponseWriter, r *http.Request) {
	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Inc(labels...)

	answer(w, r, "Incremented")
}

func (c *gauge) setHandlerFunc(w http.ResponseWriter, r *http.Request) {
	value, err := getValue(r)
	if err != nil {
		badRequest(w, r, "Missing or wrong value parameter")

		return
	}

	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Set(value, labels...)

	answer(w, r, "Set")
}
