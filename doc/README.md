# Documentation

- [Rake tasks](./rake-tasks.md) contained in this project
- [ChatOps triggers](./chatops.md) available from this project
- [Issue templates](../templates) used for creating the release task lists
- [CI variables](./variables.md) used by this project
- [Metrics](./metrics.md) contains information on developing metrics gathered
  by this project
- [Releases using the API](./api-releases.md) provides information about the
  code used for performing releases using the API

## Development

### Setup

Follow the steps described in [rake-tasks.md](rake-tasks.md#setup).

### Ruby console

Start a ruby console with:

```shell
bundle exec pry --gem
```

### Dry run mode

To execute code in dry-run mode, add the following environment variable when
executing rake tasks: `TEST='true'`.

Before performing any edit actions or any action that should not happen in dry-run
mode, consider adding a dry-run check before it. For example:

```ruby
logger.debug("About to perform edit action", relevant_data: data)

return if ReleaseTools::SharedStatus.dry_run?

# Perform edit action
```

#### Testing

By default, all tests are executed with the `TEST=true` flag already set. If you
want to verify that code guarded by a `dry_run?` check is executed, use the
`without_dry_run` helper method:

```ruby
it "executes the API call" do
  instance = described_class.new

  # Be sure to stub API calls so nothing's actually executed
  expect(ReleaseTools::GitlabClient)
    .to receive(:some_api_request)
    .and_return(true)

  without_dry_run do
    instance.execute
  end
end
```

If you don't stub the API call, [WebMock][] will block the request and issue a
test failure.

[WebMock]: https://github.com/bblimke/webmock


### Retrying API requests with Retriable

Developers should program defensively when interacting with the GitLab API by
retrying requests that fail due to timeouts or other intermittent failures.

This project utilizes the [Retriable][] gem to make this easier, and a context
has been added to simplify this common use case:

```ruby
# Automatically retry the request in the event of a `Timeout::Error`,
# `Errno::ECONNRESET`, or `Gitlab::Error::ResponseError` exception
Retriable.with_context(:api) do
  # ...
end

# Contexts support all additional Retriable parameters
Retriable.with_context(:api, retries: 10, on: StandardError) do
  # ...
end

# WARNING: Supplying `on` to `with_context` will *override* the exception list
#
# This block will only retry on `SomeRareException` and nothing else!
Retriable.with_context(:api, on: SomeRareException) do
  # ...
end
```

[Retriable]: https://github.com/kamui/retriable

### Feature flags

#### Procedure to remove a feature flag

1. Create MR to remove feature flag from code.
1. Merge MR once reviewed and approved.
1. Make sure that all pipelines started before the MR was merged have been
   completed: <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines>.

   This is required because pipelines started before the MR was merged use an older
   commit, which will look for the feature flag.

1. Delete the feature flag from <https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags>.

## Testing

Project changes must be made via [merge requests] and go through code review by
members of the Delivery team.

The project has a [fairly comprehensive][coverage] RSpec-based test suite. New
functionality should be covered by automated testing.

[merge requests]: https://gitlab.com/gitlab-org/release-tools/-/merge_requests
[coverage]: http://gitlab-org.gitlab.io/release-tools/coverage/

### Running tests

To run the full test suite, use `bundle exec rspec`. This project includes the
[Fuubar formatter](https://github.com/thekompanee/fuubar) for rapid failure
feedback:

```sh
bundle exec rspec -f Fuubar
```

Targeted tests can be executed by passing a specific file to test, and even a
specific line:

```
bundle exec rspec -f Fuubar -- spec/lib/release_tools/my_class_spec.rb:5
```

### Factories

This project makes use of [factory_bot][] to easily build stubbed objects for
testing. Instead of mimicking ActiveRecord-like objects, they're intended to
substitute `Gitlab::ObjectifiedHash` objects returned from GitLab API calls by
the `gitlab` gem. They're built as `RSpec::Mock::Double` instances.

#### Examples

```ruby
# Mimic a merge request returned by the API
merge_request = build(:merge_request, :merged, source_branch: 'add_factory_bot')
merge_request.iid  # => 1
merge_request.state  #=> "merged"
merge_request.web_url  # => "https://example.com/foo/bar/-/merge_requests/1"

# Mimic a pipeline
pipeline = build(:pipeline, :running, ref: 'main')
pipeline.id  # => 2
pipeline.status  # => "running"
pipeline.ref  # => "main"
pipeline.sha  # => "69689ed508d1c21da6091d1e90d09a87dd453f68"
```

[factory_bot]: https://github.com/thoughtbot/factory_bot
