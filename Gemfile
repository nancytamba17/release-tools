# frozen_string_literal: true

source "https://rubygems.org"

gem 'activesupport', '~> 7.0.0'
# chef-api is deprecated but brings in fewer dependencies
# than the official chef gem
gem 'chef-api', '~> 0.10'
gem 'colorize'
gem 'gitlab', '~> 4.19'
gem 'graphql-client', '~> 0.16'
gem 'http', '~> 5.1.0'
gem 'parallel', '~> 1.14'
gem 'rake'
gem 'retriable', '~> 3.1'
gem 'rugged', '~> 1.1'
gem 'semantic_logger', '~> 4.11.0'
gem 'sentry-raven', '~> 3.0', require: false
gem 'slack-ruby-block-kit', '~> 0.20.0'
gem 'tzinfo-data' # Required for CI environments where the OS might not have timezone data.
gem 'unleash', '~> 4.2.0'
gem 'version_sorter', '~> 2.3.0'

group :metrics do
  gem 'prometheus-client', '~> 2.1.0'
end

group :development, :test do
  gem 'byebug'
  gem 'climate_control', '~> 1.2.0'
  gem 'factory_bot', '~> 6.2.0'
  gem 'fuubar', require: false
  gem 'pry'
  gem 'rspec', '~> 3.8'
  gem 'rubocop', '~> 1.30.0'
  gem 'rubocop-performance', '~> 1.14.0'
  gem 'rubocop-rspec', '~> 2.11.0'
  gem 'simplecov', '~> 0.21.0'
  gem 'timecop', '~> 0.9.0'
  gem 'vcr', '~> 6.1.0'
  gem 'webmock', '~> 3.18.0'
end
