FROM golang:1.18.8-alpine as go
RUN go version

FROM ruby:2.7.6-alpine

# Set UTF-8 http://jaredmarkell.com/docker-and-locales/
ENV LANG C.UTF-8
ENV LANGUAGE C
ENV LC_ALL C.UTF-8

RUN apk add --update --no-cache \
    build-base \
    cmake \
    git \
    openssh-client \
    openssl-dev \
    && rm -rf /var/lib/apt/lists/*

# Install golang
COPY --from=go /usr/local/go/ /usr/local/go/
ENV PATH="/usr/local/go/bin:${PATH}"

RUN mkdir -p /deps
WORKDIR /deps
COPY Gemfile* /deps/
RUN gem install bundler && \
    bundle config set frozen 'true' && \
    bundle install -j $(nproc)

CMD ["/bin/sh"]