# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockerIssueFetcher
      include ::SemanticLogger::Loggable

      attr_reader :deployment_blockers, :uncategorized_incidents, :start_time, :end_time

      def initialize
        @start_time = Time.current.last_week
        @end_time = @start_time.end_of_week
        @deployment_blockers = []
        @uncategorized_incidents = []
      end

      def fetch
        @deployment_blockers =
          (release_blockers + deploys_blocked_incidents).map do |issue|
            ReleaseTools::Deployments::BlockerIssue.new(issue)
          end

        @uncategorized_incidents = remaining_incidents.map do |issue|
          ReleaseTools::Deployments::BlockerIssue.new(issue)
        end
      end

      private

      def release_blockers
        issues(
          project: ReleaseTools::Project::Release::Tasks,
          options: { labels: 'release-blocker' }
        )
      end

      def deploys_blocked_incidents
        incidents.select do |issue|
          issue.labels.grep(/Deploys-blocked/).any?
        end
      end

      def incidents
        @incidents ||=
          issues(
            project: ReleaseTools::Project::Infrastructure::Production,
            options: { labels: 'incident' }
          )
      end

      def remaining_incidents
        incidents.reject do |issue|
          issue.labels.grep(/Deploys-blocked/).any?
        end
      end

      def issues(project:, options: {})
        options[:created_after] = start_time
        options[:created_before] = end_time

        logger.info('Fetching issues', project: project, options: options)

        ReleaseTools::GitlabClient
          .issues(project, options)
          .auto_paginate
      end
    end
  end
end
