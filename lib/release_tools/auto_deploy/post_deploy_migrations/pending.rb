# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class Pending
        include ::SemanticLogger::Loggable

        JOB_NAME = 'build-post-deploy-migrations-artifact'
        RELEASE_TOOLS_USERNAME = 'gitlab-release-tools-bot'

        def initialize
          @client = ReleaseTools::GitlabOpsClient
          @project = ReleaseTools::Project::ReleaseTools
        end

        def execute
          project_pipelines.each do |pipeline|
            job = find_post_deploy_job(pipeline.id)

            next unless job

            return fetch_pending_post_migrations_artifact(pipeline.id, job.id)
          end

          logger.info('No pipeline nor job could be found')

          []
        end

        private

        attr_reader :client, :project

        def project_pipelines
          options = {
            username: RELEASE_TOOLS_USERNAME,
            status: 'success',
            source: 'trigger'
          }

          logger.info('Retrieving pipelines', project: project)

          Retriable.with_context(:api) do
            client.pipelines(project, options)
          end
        end

        def find_post_deploy_job(pipeline_id)
          logger.info('Searching for build-post-deploy-migrations-artifact job', pipeline: pipeline_id)

          Retriable.with_context(:api) do
            client.pipeline_job_by_name(
              project,
              pipeline_id,
              JOB_NAME
            )
          end
        end

        def fetch_pending_post_migrations_artifact(pipeline_id, job_id)
          logger.info('Fetching pending post migration artifact', pipeline: pipeline_id, job: job_id)

          pending_post_migrations = Retriable.with_context(:api) do
            client.download_raw_job_artifact(
              project,
              job_id,
              PostDeployMigrations::Artifact::FILE_NAME
            )
          end

          return [] unless pending_post_migrations.present?

          pending_post_migrations.split(',')
        rescue Gitlab::Error::NotFound
          []
        end
      end
    end
  end
end
