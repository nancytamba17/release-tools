# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      # Notifier sends notifications for post-deploy migrations pipelines.
      class Notifier
        include ReleaseTools::AutoDeploy::Pipeline
        include ::SemanticLogger::Loggable

        def initialize(pipeline_id:, environment:)
          @pipeline_id = pipeline_id
          @environment = environment
        end

        def execute
          post_migrations_pipeline = find_downstream_pipeline

          if post_migrations_pipeline
            logger.info('Post-migrations pipeline found', post_migrations_pipeline: post_migrations_pipeline.web_url)

            send_slack_notification(post_migrations_pipeline)
          else
            logger.fatal('Post-migrations pipeline not found')
          end
        end

        private

        attr_reader :pipeline_id, :environment

        def find_downstream_pipeline
          logger.info('Fetching post-migrations downstream pipeline', environment: environment, pipeline_id: pipeline_id)

          Retriable.with_context(:pipeline_created) do
            downstream_pipeline = pipeline_bridges
              .find { |job| job.name == "post_deploy_migrations:#{environment}" }
              &.downstream_pipeline

            raise MissingPipelineError if downstream_pipeline.nil?

            downstream_pipeline
          end
        rescue MissingPipelineError
          nil
        end

        def pipeline_bridges
          ReleaseTools::GitlabOpsClient.pipeline_bridges(Project::ReleaseTools, pipeline_id)
        end

        def send_slack_notification(post_deploy_pipeline)
          return if SharedStatus.dry_run?

          options = {
            pipeline: post_deploy_pipeline,
            environment: environment
          }

          ReleaseTools::Slack::PostDeployMigrationsNotification.new(options).execute
        end
      end
    end
  end
end
