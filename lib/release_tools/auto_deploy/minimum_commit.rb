# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    # Finding of the minimum/oldest commit to tag during an auto-deploy tagging
    # run.
    class MinimumCommit
      include ::SemanticLogger::Loggable

      TAGS_PROJECT = Project::ReleaseTools.ops_path

      def initialize(project)
        @project = project
      end

      def sha
        tag = GitlabOpsClient
          .tags(TAGS_PROJECT, sort: 'desc', order_by: 'updated', per_page: 1)
          .first

        unless tag
          logger.warn('No tag found', project: TAGS_PROJECT)

          return
        end

        ProductVersion.new(tag.name)[@project.metadata_project_name]&.sha.tap do |sha|
          logger.info('Minimum commit pulled from metadata', tag: tag.name, sha: sha)
        end
      end
    end
  end
end
