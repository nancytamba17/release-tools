# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Qa
        # Notifier sends notifications for failed downstream QA pipelines.
        class Notifier
          include ::SemanticLogger::Loggable
          include AutoDeploy::QaNotifier

          def initialize(pipeline_id:, deploy_version:, environment:)
            @pipeline_id    = pipeline_id
            @deploy_version = deploy_version
            @environment    = environment
            @qa_smoke_jobs  = ["qa:smoke:#{environment}", "qa:smoke-main:#{environment}"]
          end
        end
      end
    end
  end
end
