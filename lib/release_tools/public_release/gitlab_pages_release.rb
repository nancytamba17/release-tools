# frozen_string_literal: true

module ReleaseTools
  module PublicRelease
    class GitlabPagesRelease
      include Release

      attr_reader :version, :client, :release_metadata

      def initialize(
        version,
        client: GitlabClient,
        release_metadata: ReleaseMetadata.new,
        commit: nil
      )
        @version = version.to_ce
        @client = client
        @release_metadata = release_metadata
        @commit = commit
      end

      def execute
        logger.info('Starting release of GitLab Pages', version: version)

        create_target_branch
      end

      def create_target_branch
        source = source_for_target_branch

        logger.info(
          'Creating target branch',
          project: project_path,
          source: source
        )

        nil
      end

      def source_for_target_branch
        if @commit
          logger.info('Using specific commit', project: project_path, commit: @commit)
        end

        @commit || last_production_commit_metadata
      end

      private

      def project
        Project::GitlabPages
      end
    end
  end
end
