# frozen_string_literal: true

require_relative '../support/ubi_helper'
require_relative '../support/fips_helper'

module ReleaseTools
  module Services
    class SyncRemotesService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Services::SyncRefsHelper
      include ReleaseTools::Support::UbiHelper
      include ReleaseTools::Support::FIPSHelper

      def initialize(version)
        @version = version.to_ce
        @omnibus = OmnibusGitlabVersion.new(@version.to_omnibus)
        @helm    = ReleaseTools::Helm::HelmVersionFinder.new.execute(version)
      end

      def execute
        sync_stable_branches_for_projects
        sync_tags_for_projects
      end

      # Syncs stable branches for GitLab, GitLab FOSS, Omnibus GitLab,
      # CNG Image and Gitaly
      #
      # - For GitLab ee stable branches (*-*-stable-ee) are synced
      # - For GitLab FOSS, Omnibus GitLab and Gitaly, stable branches (*-*-stable) are synced
      #   Omnibus uses a single branch post 12.2
      # - For CNGImage, ee and regular stable branches are synced.
      def sync_stable_branches_for_projects
        sync_branches(Project::GitlabCe, @version.stable_branch(ee: false))
        sync_branches(Project::GitlabEe, @version.stable_branch(ee: true))
        sync_branches(Project::OmnibusGitlab, @omnibus.to_ce.stable_branch)
        sync_branches(Project::CNGImage, @version.to_ce.stable_branch)
        sync_branches(Project::Gitaly, @version.stable_branch(ee: false))
        sync_branches(Project::HelmGitlab, @helm.stable_branch)
      end

      def sync_tags_for_projects
        sync_tags(Project::GitlabCe, @version.tag(ee: false))
        sync_tags(Project::GitlabEe, @version.tag(ee: true))
        sync_tags(Project::OmnibusGitlab, @omnibus.to_ee.tag, @omnibus.to_ce.tag)

        cng_tags = [@version.to_ce.tag, @version.to_ee.tag, ubi_tag(@version.to_ee)].tap do |versions|
          versions << fips_tag(@version.to_ee) if @version >= Version.new(GITLAB_FIPS_MINIMUM_VERSION)
        end
        sync_tags(Project::CNGImage, *cng_tags)

        sync_tags(Project::Gitaly, @version.tag(ee: false))
        sync_tags(Project::HelmGitlab, @helm.tag)
      end
    end
  end
end
