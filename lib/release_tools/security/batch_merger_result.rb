# frozen_string_literal: true

module ReleaseTools
  module Security
    class BatchMergerResult
      attr_accessor :processed, :unprocessed

      def initialize
        @processed = []
        @unprocessed = []
      end

      def build_attachments
        return [] if @processed.empty? && @unprocessed.empty?

        attachments = [
          {
            type: 'divider'
          }
        ]

        attachments.concat(processed_attachments)
        attachments.concat(unprocessed_attachments)
      end

      private

      def processed_attachments
        return [] if @processed.empty?

        blocks = ::Slack::BlockKit.blocks
        blocks.section { |section| section.mrkdwn(text: "*:white_check_mark: Processed:*") }

        issues_details(@processed, blocks)

        blocks.as_json
      end

      def unprocessed_attachments
        return [] if @unprocessed.empty?

        blocks = ::Slack::BlockKit.blocks
        blocks.section { |section| section.mrkdwn(text: "*:red_circle: Unprocessed:*") }

        issues_details(@unprocessed, blocks)

        blocks.as_json
      end

      def issues_details(issues, blocks)
        issues.each do |issue|
          line = "• <#{issue.web_url}|#{issue.reference}>"

          blocks.context { |block| block.mrkdwn(text: line) }
        end
      end
    end
  end
end
