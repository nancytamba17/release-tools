# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class MirrorStatus
      include ::SemanticLogger::Loggable

      METRIC = :delivery_mirror_ok
      DESCRIPTION = "Status of Security mirror chains"
      LABELS = %i[project type].freeze

      SECURITY_MIRROR_PATTERN = 'gitlab-org/security'
      BUILD_MIRROR_PATTERN = 'dev.gitlab.org'

      def initialize
        @prometheus = ReleaseTools::Prometheus::Query.new

        @registry = ::Prometheus::Client::Registry.new
        @registry.unregister(METRIC)

        @push = Metrics::Push.new(METRIC)
        @metric = @registry.gauge(METRIC, docstring: DESCRIPTION, labels: LABELS)
      end

      def execute
        Parallel.each(security_forks, in_threads: Etc.nprocessors) do |security_fork|
          canonical = security_fork['forked_from_project']

          security = mirror_status(canonical['id'], SECURITY_MIRROR_PATTERN)
          build = mirror_status(security_fork['id'], BUILD_MIRROR_PATTERN)

          if security
            status = security.last_error.nil? ? 1 : 0
            labels = LABELS.zip([canonical['path_with_namespace'], 'security']).to_h

            logger.info('Mirror status', labels.merge(status: status.to_s))
            @metric.set(status, labels: labels)
          end

          if build # rubocop:disable Style/Next
            status = build.last_error.nil? ? 1 : 0
            labels = LABELS.zip([canonical['path_with_namespace'], 'build']).to_h

            logger.info('Mirror status', labels.merge(status: status.to_s))
            @metric.set(status, labels: labels)
          end
        end

        @push.replace(@registry)
      end

      private

      def security_forks
        @security_forks ||= ReleaseTools::GitlabClient
          .group_projects('gitlab-org/security', include_subgroups: true)
          .map(&:to_h)
          .select { |p| p.key?('forked_from_project') }
      end

      # Given a project ID, find a remote mirror with a URL matching a given pattern
      def mirror_status(project_id, pattern)
        ReleaseTools::GitlabClient
          .remote_mirrors(project_id)
          .detect { |m| m.url.include?(pattern) }
      rescue ::Gitlab::Error::Unauthorized
        # gitlab-release-tools-bot isn't a Maintainer on the project
        nil
      end
    end
  end
end
