# frozen_string_literal: true

module ReleaseTools
  module Project
    module Quality
      class GitlabQa < Base
        REMOTES = {
          canonical: 'git@gitlab.com:gitlab-org/gitlab-qa.git',
          ops: 'git@ops.gitlab.net:gitlab-org/gitlab-qa.git'
        }.freeze

        def self.requires_qa_issue?
          true
        end

        def self.environment
          'gstg'
        end
      end
    end
  end
end
