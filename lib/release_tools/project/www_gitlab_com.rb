# frozen_string_literal: true

module ReleaseTools
  module Project
    class WWWGitlabCom < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-com/www-gitlab-com.git',
        security:  'git@gitlab.com:gitlab-com/security/www-gitlab-com.git',
        dev:       'git@dev.gitlab.org:gitlab/www-gitlab-com.git'
      }.freeze
    end
  end
end
