# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      class ActiveGprdDeployments
        include ::ReleaseTools::Promotion::Check
        include ActiveDeployments

        def env
          ::ReleaseTools::PublicRelease::Release::PRODUCTION_ENVIRONMENT
        end
      end
    end
  end
end
