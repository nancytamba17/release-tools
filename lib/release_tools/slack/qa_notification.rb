# frozen_string_literal: true

module ReleaseTools
  module Slack
    class QaNotification
      include ::SemanticLogger::Loggable
      include Utilities

      QaPipeline = Struct.new(:name, :web_url, keyword_init: true) do
        def type
          if name.include?('smoke-main')
            'smoke-main'
          else
            'smoke'
          end
        end
      end

      def initialize(deploy_version:, pipelines:, environment:)
        @deploy_version = deploy_version
        @pipelines = build_qa_pipelines(pipelines)
        @environment = environment
      end

      def execute
        logger.info('Sending qa slack notification', deploy_version: deploy_version, environment: environment, quality_url: pipelines.map(&:web_url).map(&:to_s), slack_channel: slack_channel)

        return if SharedStatus.dry_run?

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        )
      end

      private

      attr_reader :deploy_version, :pipelines, :environment, :slack_channel

      def build_qa_pipelines(pipelines)
        pipelines.map do |pipeline|
          QaPipeline.new(name: pipeline.name, web_url: pipeline.downstream_pipeline.web_url)
        end
      end

      def fallback_text
        "qa #{environment} failed #{deploy_version}"
      end

      def slack_block
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: [clock_context_element]
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << prefix
          text << block_message
        end.join(' ')
      end

      def status_icon
        STATUS_ICONS[:failed]
      end

      def prefix
        "*QA #{environment}*"
      end

      def block_message
        msg =
          if pipelines.one?
            "*#{pipelines.first.type}* <#{pipelines.first.web_url}|failed>"
          else
            links = pipelines.map { |p| "<#{p.web_url}|#{p.type}>" }.to_sentence

            "#{links} failed"
          end

        msg += " `#{deploy_version}`" if deploy_version

        msg
      end
    end
  end
end
