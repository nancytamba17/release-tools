# frozen_string_literal: true

module ReleaseTools
  module Tracker
    # rubocop: disable Metrics/ModuleLength
    # An interface for tracking deployments, it defines methods
    # for tracking two types of deployments:
    #
    # * API deployments for given environments
    # * Product deployments on gitlab-org/release/metadata
    #
    # It expects for three attributes to be defined: version, status
    # and environment
    module Deployment
      # The Metadata Project
      METADATA_PROJECT = Project::Release::Metadata

      # The deployment statuses that we support.
      DEPLOYMENT_STATUSES = Set.new(%w[running success failed]).freeze

      # A deployment created using the GitLab API
      Deployment = Struct.new(:project_path, :id, :status) do
        def success?
          status == 'success'
        end
      end

      # Tracks the deployment for different components on a given environment, by:
      # 1. Ensuring required checks are complied
      # 2. Tracking on security repository by default, this is because the auto-deploy
      # branches only exist on security.
      # 3. Tracking on canonical repository if necessary, it finds an auto_deploy
      # intersection between the security auto_deploy branch and the canonical default
      # branch. This is required to track canonical merge requests
      # and merge requests processed by the pick-into-auto-deploy
      def track
        perform_checks

        deployments = []

        versions.each_entry do |project, version|
          deployments << track_deployment_on_security(project, version)

          canonical_sha = auto_deploy_intersection(project, version.sha)
          next unless canonical_sha

          deployments << track_deployment_on_canonical(project, version, canonical_sha)
        end

        deployments
      end

      def record_metadata_deployment
        perform_checks

        begin
          create_product_deployment
        rescue StandardError => ex
          ::Raven.capture_exception(ex)
        end
      end

      private

      # Defines checks to be performed before tracking the deployments
      def perform_checks
        raise NotImplementedError
      end

      def check_status
        return if DEPLOYMENT_STATUSES.include?(status)

        raise ArgumentError, "The status #{status} is not supported"
      end

      # The product version associated with an auto-deploy package
      def product_version
        @product_version ||= ProductVersion.from_package_version(version)
      end

      # Defines component versions to be tracked. It should return an array of
      # arrays, each array should define:
      #
      # * The project to track the environment, and,
      # * the respective version.
      #
      # Example:
      #
      # gitlab_version = product_version['gitlab-ee']
      # omnibus_version = product_version['omnibus-gitlab-ee']
      # gitaly_version = product_version['gitaly']
      #
      # [
      #   [Project::GitlabEe, gitlab_version],
      #   [Project::OmnibusGitlab, omnibus_version],
      #   [Project::Gitaly, gitaly_version]
      # ]
      def versions
        raise NotImplementedError
      end

      def track_deployment_on_security(project, version)
        ref = version.ref
        sha = version.sha
        is_tag = version.tag?

        log_previous_deployment(project.auto_deploy_path)

        logger.info(
          "Recording #{project.name.demodulize} deployment",
          environment: environment,
          status: status,
          sha: sha,
          ref: ref
        )

        return if SharedStatus.dry_run?

        data =
          Retriable.with_context(:api) do
            GitlabClient.update_or_create_deployment(
              project.auto_deploy_path,
              environment,
              ref: ref,
              sha: sha,
              status: status,
              tag: is_tag
            )
          end

        log_new_deployment(project.auto_deploy_path, data)

        Deployment.new(project.auto_deploy_path, data.id, data.status)
      end

      def log_previous_deployment(project_path)
        deploy = Retriable.with_context(:api) do
          GitlabClient.last_successful_deployment(project_path, environment)
        end

        return unless deploy

        logger.info(
          "Previous deployment for #{project_path} is deploy ##{deploy.iid}",
          id: deploy.id,
          iid: deploy.iid,
          ref: deploy.ref,
          sha: deploy.sha
        )
      end

      def log_new_deployment(project_path, deploy)
        logger.info(
          "Created new deployment for #{project_path}",
          path: project_path,
          id: deploy.id,
          iid: deploy.iid,
          ref: deploy.ref,
          sha: deploy.sha
        )
      end

      # Find the first commit that exists in the Security auto-deploy branch and
      # the Canonical default branch
      #
      # The commit being deployed in the auto-deploy branch is usually something
      # that only exists in that branch, and thus can't be used for a Canonical
      # deployment.
      def auto_deploy_intersection(project, sha)
        canonical = GitlabClient
          .commits(project.path, ref_name: project.default_branch, per_page: 100)
          .paginate_with_limit(300)
          .collect(&:id)

        GitlabClient.commits(project.auto_deploy_path, ref_name: sha).paginate_with_limit(300) do |commit|
          return commit.id if canonical.include?(commit.id)
        end

        logger.warn(
          'Failed to find auto-deploy intersection',
          project: project.path,
          sha: sha
        )

        nil
      rescue Gitlab::Error::Error => ex
        logger.warn(
          'Failed to find auto-deploy intersection',
          project: project.path,
          sha: sha,
          error: ex.message
        )

        nil
      end

      # Merge requests on Canonical still need to be notified about
      # the deployment, but the given ref won't exist there, so we also create
      # one on Canonical using its default branch and the first commit that
      # exists on both Security and Canonical.
      def track_deployment_on_canonical(project, version, canonical_sha)
        sha = version.sha
        is_tag = version.tag?

        log_previous_deployment(project.path)

        logger.info(
          "Recording #{project.name.demodulize} Canonical deployment",
          environment: environment,
          status: status,
          sha: sha,
          ref: project.default_branch
        )

        return if SharedStatus.dry_run?

        data = GitlabClient.update_or_create_deployment(
          project.path,
          environment,
          ref: project.default_branch,
          sha: canonical_sha,
          status: status,
          tag: is_tag
        )

        log_new_deployment(project.path, data)

        Deployment.new(project.path, data.id, data.status)
      end

      def create_product_deployment
        logger.info(
          "Recording Product deployment",
          environment: environment,
          status: status,
          version: version,
          sha: product_version.metadata_commit_id
        )

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          GitlabOpsClient.update_or_create_deployment(
            METADATA_PROJECT,
            environment,
            ref: METADATA_PROJECT.default_branch,
            sha: product_version.metadata_commit_id,
            status: status
          )
        end
      end
      # rubocop: enable Metrics/ModuleLength
    end
  end
end
