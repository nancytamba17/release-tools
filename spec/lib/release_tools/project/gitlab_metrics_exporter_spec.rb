# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::GitlabMetricsExporter do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .security_group', 'gitlab-org/security'
  it_behaves_like 'project .security_path', 'gitlab-org/security/gitlab-metrics-exporter'
  it_behaves_like 'project .to_s'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-org/gitlab-metrics-exporter' }
  end

  describe '.dev_path' do
    it { expect(described_class.dev_path).to eq 'gitlab/gitlab-metrics-exporter' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-org' }
  end

  describe '.dev_group' do
    it { expect(described_class.dev_group).to eq 'gitlab' }
  end

  describe '.version_file' do
    it { expect(described_class.version_file).to eq 'GITLAB_METRICS_EXPORTER_VERSION' }
  end

  describe '.metadata_project_name' do
    it { expect(described_class.metadata_project_name).to eq 'gitlab_metrics_exporter' }
  end
end
