# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SyncRemotesService do
  describe '#execute' do
    let(:other_args) { ['master', { create_mr_on_merge_failure: true }] }

    it 'syncs master branch for every project' do
      service = described_class.new

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabEe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabCe, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::OmnibusGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::Gitaly, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::HelmGitlab, *other_args)

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabOperator, *other_args)

      service.execute
    end
  end
end
