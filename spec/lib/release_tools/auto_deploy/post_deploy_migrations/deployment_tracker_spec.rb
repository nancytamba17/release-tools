# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::DeploymentTracker do
  let(:environment) { 'db/gstg' }
  let(:status) { 'running' }
  let(:version) { '15.1.202206061520-f5556ff4fcd.82285983410' }

  let(:product_version) do
    ReleaseTools::ProductVersion.from_package_version(version)
  end

  let(:product_meta) do
    {
      'releases' => {
        'gitlab-ee' => {
          'ref' => '1-2-auto-deploy-01022020',
          'sha' => '123',
          'tag' => false
        }
      }
    }
  end

  let(:deployments_metadata) { double(:meta, security_release?: false) }
  let(:previous_deployment) { double(:deployment, id: 1, iid: 1, ref: 'master', sha: 'abc') }

  subject(:tracker) do
    described_class.new(environment: environment, status: status, version: version)
  end

  before do
    allow(ReleaseTools::ProductVersion)
      .to receive(:from_package_version)
      .and_return(product_version)

    allow(product_version)
      .to receive(:metadata)
      .and_return(product_meta)

    allow(ReleaseTools::Deployments::Metadata)
      .to receive(:new)
      .and_return(deployments_metadata)

    allow(ReleaseTools::GitlabClient)
      .to receive(:last_successful_deployment)
      .and_return(create(:deployment, :success))

    allow(ReleaseTools::GitlabClient)
      .to receive(:update_or_create_deployment)
      .and_return(create(:deployment, :success))

    allow(tracker)
      .to receive(:auto_deploy_intersection)
      .and_return(nil)
  end

  describe '#track' do
    it 'tracks the deployment on security by default' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:update_or_create_deployment)
        .with(
          'gitlab-org/security/gitlab',
          'db/gstg',
          ref: '1-2-auto-deploy-01022020',
          sha: '123',
          status: 'running',
          tag: false
        ).and_return(build(:deployment, id: 1, sha: '123'))

      without_dry_run { tracker.track }
    end

    context 'when the commit exists on canonical' do
      it 'tracks the deployment on security and canonical' do
        allow(tracker)
          .to receive(:auto_deploy_intersection)
          .and_return('abcde')

        expect(tracker).to receive(:auto_deploy_intersection)
          .with(ReleaseTools::Project::GitlabEe, '123')

        expect(ReleaseTools::GitlabClient)
          .to receive(:update_or_create_deployment)
          .with(
            'gitlab-org/security/gitlab',
            'db/gstg',
            ref: '1-2-auto-deploy-01022020',
            sha: '123',
            status: 'running',
            tag: false
          ).and_return(build(:deployment, id: 1, sha: '123'))

        expect(ReleaseTools::GitlabClient)
          .to receive(:update_or_create_deployment)
          .with(
            'gitlab-org/gitlab',
            'db/gstg',
            ref: 'master',
            sha: 'abcde',
            status: 'running',
            tag: false
          ).and_return(build(:deployment, id: 1, sha: 'abcde'))

        without_dry_run { tracker.track }
      end
    end

    context 'with a security release' do
      let(:deployments_metadata) do
        double(:meta, security_release?: true)
      end

      it 'tracks the execution on the security repo' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:update_or_create_deployment)
          .with(
            'gitlab-org/security/gitlab',
            'db/gstg',
            ref: '1-2-auto-deploy-01022020',
            sha: '123',
            status: 'running',
            tag: false
          ).and_return(build(:deployment, id: 1, sha: '123'))

        without_dry_run { tracker.track }
      end
    end

    context 'with an invalid environment' do
      let(:environment) { 'foo' }

      it 'raises an error' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:update_or_create_deployment)

        expect do
          tracker.track
        end.to raise_error('The environment foo is not supported')
      end
    end

    context 'with an invalid status' do
      let(:status) { 'canceled' }

      it 'raises an error' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:update_or_create_deployment)

        expect do
          tracker.track
        end.to raise_error('The status canceled is not supported')
      end
    end

    context 'with dry run' do
      it 'does not create deployment' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:update_or_create_deployment)

        tracker.track
      end
    end
  end

  describe '#record_metadata_deployment' do
    it 'tracks a production deployment' do
      metadata_commit_id = '123abc'

      allow(product_version)
        .to receive(:metadata_commit_id)
        .and_return(metadata_commit_id)

      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:update_or_create_deployment)
        .with(
          described_class::METADATA_PROJECT,
          'db/gstg',
          ref: 'master',
          sha: metadata_commit_id,
          status: 'running'
        )

      without_dry_run { tracker.record_metadata_deployment }
    end

    context 'with an invalid status' do
      let(:status) { 'running' }

      it 'does not create a deployment' do
        expect(ReleaseTools::GitlabOpsClient)
          .not_to receive(:update_or_create_deployment)

        without_dry_run { tracker.record_metadata_deployment }
      end
    end

    context 'with a dry-run' do
      it 'does not create a deployment' do
        allow(product_version)
          .to receive(:metadata_commit_id)
          .and_return('123abc')

        expect(ReleaseTools::GitlabOpsClient)
          .not_to receive(:update_or_create_deployment)

        tracker.record_metadata_deployment
      end
    end
  end
end
