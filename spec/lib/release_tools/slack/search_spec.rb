# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::Search do
  describe '.query' do
    let(:slack_channel) { 'foo' }
    let(:query) { 'baz' }

    context 'without credentials' do
      it 'raises NoCredentialsError' do
        ClimateControl.modify(SLACK_APP_USER_TOKEN: nil) do
          expect do
            described_class.query(query)
          end.to raise_error(described_class::NoCredentialsError)
        end
      end
    end

    context 'with credentials' do
      let(:fake_client) { double(:client) }
      let(:query) { "New coordinated pipeline: 14.9.202203180306" }
      let(:full_query) { query }

      let(:json_response) do
        {
          ok: true,
          query: full_query,
          messages: {
            total: 465,
            pagination: { total_count: 465, page: 1, per_page: 20, page_count: 24, first: 1, last: 20 },
            paging: { count: 20, total: 465, page: 1, pages: 24 },
            matches: [{
              iid: '74bd228e-c2e4',
              team: 'team',
              score: 2067.0996,
              channel: { id: 'id', is_channel: true, name: "f_upcoming_release" },
              type: "message",
              user: "U9H1JFDBM",
              username: "gitlab-chatops",
              ts: "1647572855.397269",
              text: "New coordinated pipeline: <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/1098871|14.9.202203180306>",
              permalink: "slack.permalink",
              no_reactions: true
            }]
          }
        }.deep_stringify_keys
      end

      let(:response) do
        instance_spy(
          HTTP::Response,
          status: double(:status, success?: true),
          code: 200,
          reason: 'OK',
          parse: json_response
        )
      end

      before do
        allow(described_class).to receive(:client).and_return(fake_client)

        allow(fake_client).to receive(:post).and_return(response)
      end

      context 'with channel parameter' do
        let(:full_query) { "New coordinated pipeline: 14.9.202203180306 in:#{slack_channel}" }

        it 'searches for a message on a specific Slack channel' do
          expect(fake_client)
            .to receive(:post)
            .with(
              described_class::API_URL,
              form: {
                query: include("in:#{slack_channel}")
              }
            )

          ClimateControl.modify(SLACK_APP_USER_TOKEN: '123456') do
            expect(described_class.query(query, channel: slack_channel)).to eq(json_response.dig('messages', 'matches'))
          end
        end
      end

      context 'with from_user parameter' do
        let(:full_query) { "New coordinated pipeline: 14.9.202203180306 from:@gitlab-chatops" }

        it 'searches for a message by a specific user or bot' do
          user = 'username1'

          expect(fake_client)
            .to receive(:post)
            .with(
              described_class::API_URL,
              form: {
                query: include("from:@#{user}")
              }
            )

          ClimateControl.modify(SLACK_APP_USER_TOKEN: '123456') do
            described_class.query(query, from_user: user)
          end
        end
      end

      context 'when response is not ok' do
        let(:json_response) { { ok: false, error: 'invalid_auth' }.deep_stringify_keys }

        around do |ex|
          ClimateControl.modify(SLACK_APP_USER_TOKEN: '123456') do
            ex.run
          end
        end

        it 'raises CouldNotSearchError' do
          expect { described_class.query(query, channel: slack_channel) }
            .to raise_error(described_class::CouldNotSearchError)
        end
      end

      context 'when response is not successful' do
        let(:response) do
          instance_spy(
            HTTP::Response,
            status: double(:status, success?: false),
            code: 400,
            reason: 'Bad Request',
            body: nil
          )
        end

        around do |ex|
          ClimateControl.modify(SLACK_APP_USER_TOKEN: '123456') do
            ex.run
          end
        end

        it 'raises CouldNotSearchError' do
          expect { described_class.query(query, channel: slack_channel) }
            .to raise_error(described_class::CouldNotSearchError)
        end
      end
    end
  end
end
