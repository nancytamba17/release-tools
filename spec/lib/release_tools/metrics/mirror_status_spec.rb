# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::MirrorStatus do
  include_context 'metric registry'

  subject(:service) { described_class.new }

  let(:fake_client) do
    stub_const('ReleaseTools::GitlabClient', class_spy(ReleaseTools::GitlabClient))
  end
  let(:security_project) { build(:project, :fork) }
  let(:canonical_project) { security_project.forked_from_project }

  describe '#execute' do
    it 'records mirror status for security projects' do
      gauge = stub_gauge(registry)
      security_mirror = build(:remote_mirror, url: "https://gitlab.com/gitlab-org/security/foo.git")
      dev_mirror = build(:remote_mirror, :failed, url: "https://dev.gitlab.org/foo/bar.git")

      allow(fake_client).to receive(:group_projects).and_return([security_project])

      # Security mirror is queried on the project we forked from
      expect(fake_client).to receive(:remote_mirrors)
        .with(security_project.forked_from_project['id'])
        .and_return([security_mirror])

      # Build mirror is queried on the Security project
      expect(fake_client).to receive(:remote_mirrors)
        .with(security_project['id'])
        .and_return([dev_mirror])

      service.execute

      # Security mirror is healthy (1)
      expect(gauge).to have_received(:set).with(
        1, labels: hash_including(project: canonical_project.path_with_namespace, type: 'security')
      )

      # Build mirror is unhealthy (0)
      expect(gauge).to have_received(:set).with(
        0, labels: hash_including(project: canonical_project.path_with_namespace, type: 'build')
      )

      expect(pushgateway).to have_received(:replace).with(registry)
    end
  end
end
