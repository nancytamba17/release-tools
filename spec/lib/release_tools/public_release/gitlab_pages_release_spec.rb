# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::GitlabPagesRelease do
  include MetadataHelper

  let(:version) { ReleaseTools::Version.new('42.0.0') }
  let(:client) { class_spy(ReleaseTools::GitlabClient) }

  subject(:release) { described_class.new(version) }

  before do
    stub_const('ReleaseTools::GitlabClient', client)
  end

  describe '#execute' do
    it 'runs the release' do
      expect(release).to receive(:create_target_branch)

      release.execute
    end
  end

  describe '#create_target_branch' do
    # TODO: to remove. This is a temporary dry-run implementation
    it 'does not create the branch' do
      allow(release).to receive(:source_for_target_branch)

      expect(release.create_target_branch).to be_nil
    end
  end

  describe '#source_for_target_branch' do
    context 'with a specific commit' do
      it 'returns the commit' do
        release = described_class.new(version, commit: 'foo')

        expect(release.source_for_target_branch).to eq('foo')
      end
    end

    it 'delegates to last_production_commit_metadata' do
      sha = 'abc123'
      expect(release).to receive(:last_production_commit_metadata).and_return(sha)
      expect(release).not_to receive(:last_production_commit_deployments)

      expect(release.source_for_target_branch).to eq(sha)
    end
  end
end
