# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::GitalyRelease do
  let(:release) do
    Class.new do
      include ReleaseTools::PublicRelease::Release
      include ReleaseTools::PublicRelease::GitalyRelease

      attr_reader :version, :client, :release_metadata

      def initialize
        @version = ReleaseTools::Version.new('42.0.0-rc42')
        @client = ReleaseTools::GitlabClient
        @release_metadata = ReleaseTools::ReleaseMetadata.new
      end

      def target_branch
        'master'
      end
    end.new
  end

  describe '#update_versions' do
    shared_examples 'a version file update' do
      it 'updates the version files' do
        expect(release)
          .to receive(:commit_version_files)
          .with('master', expected_updates, skip_ci: true)

        release.update_versions
      end
    end

    context ':skip_updating_gitaly_version_rb disabled' do
      let(:skip_updating) { false }
      let(:expected_updates) do
        {
          'VERSION' => '42.0.0-rc42',
          'ruby/proto/gitaly/version.rb' => a_string_including("VERSION = '42.0.0-rc42'")
        }
      end

      before do
        disable_feature(:skip_updating_gitaly_version_rb)
      end

      it_behaves_like 'a version file update'
    end

    context ':skip_updating_gitaly_version_rb enabled' do
      let(:expected_updates) do
        {
          'VERSION' => '42.0.0-rc42'
        }
      end

      before do
        enable_feature(:skip_updating_gitaly_version_rb)
      end

      it_behaves_like 'a version file update'
    end
  end

  describe '#create_tag' do
    it 'creates the Git tag' do
      expect(release.client)
        .to receive(:find_or_create_tag)
        .with(
          release.project_path,
          'v42.0.0-rc42',
          'master',
          message: 'Version v42.0.0-rc42'
        )

      release.create_tag
    end
  end

  describe '#add_release_metadata' do
    it 'adds the Gitaly release metadata' do
      tag =
        double(:tag, name: 'v42.0.0-rc42', commit: double(:commit, id: '123'))

      expect(release.release_metadata)
        .to receive(:add_release)
        .with(
          name: 'gitaly',
          version: '42.0.0-rc42',
          sha: '123',
          ref: 'v42.0.0-rc42',
          tag: true
        )

      release.add_release_metadata(tag)
    end
  end

  describe '#project' do
    it 'returns the project to use for releases' do
      expect(release.project).to eq(ReleaseTools::Project::Gitaly)
    end
  end
end
