# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::Release::Prepare do
  describe '#execute' do
    subject(:execute) { instance.execute }

    let(:instance) { described_class.new(version) }
    let(:issue) { instance_spy(ReleaseTools::Tasks::Release::Issue, execute: nil) }

    before do
      allow(ReleaseTools::Tasks::Release::Issue)
        .to receive(:new)
        .with(version)
        .and_return(issue)
    end

    context 'with patch version' do
      let(:version) { ReleaseTools::Version.new('14.1.9') }
      let(:patch_issue) { ReleaseTools::PatchRelease::Issue.new(version: version) }
      let(:patch_blog_mr_url) { 'https://dummy-merge-request.url' }

      before do
        allow(issue).to receive(:release_issue).and_return(patch_issue)

        allow(instance).to receive(:create_or_show_merge_request)
        allow(instance)
          .to receive(:create_or_show_merge_request)
          .with(instance_of(ReleaseTools::PatchRelease::BlogMergeRequest))
          .and_return(double(web_url: patch_blog_mr_url))

        allow(patch_issue).to receive(:add_blog_mr_to_description)
      end

      it 'creates issue' do
        expect(ReleaseTools::Tasks::Release::Issue)
          .to receive(:new)
          .with(version)

        expect(issue).to receive(:execute)

        execute
      end

      it 'creates patch blog merge request' do
        expect(instance)
          .to receive(:create_or_show_merge_request)
          .with(instance_of(ReleaseTools::PatchRelease::BlogMergeRequest))

        execute
      end

      it 'adds MR URL to patch issue description' do
        expect(patch_issue)
          .to receive(:add_blog_mr_to_description)
          .with(patch_blog_mr_url)

        execute
      end
    end
  end
end
